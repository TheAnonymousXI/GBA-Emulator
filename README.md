# Welcome to GBA Emulator

GBA Emulator is a free, online GBA emulator! I have 50+ Games to play!

CONTROLS:
X = A
Z = B
SELECT = RSHIFT/LSHIFT
START = ENTER
USE ARROW KEYS/WASD FOR MOVEMENT

# GBA Emulator Website

https://theanonymousxalt.github.io/GBA-Emulator/
